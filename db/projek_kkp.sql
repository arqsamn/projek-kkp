-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2023 at 03:38 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projek_kkp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `alamat`, `telepon`, `email`, `username`, `password`) VALUES
(1, 'admin', 'Jakarta, Indonesia', '081511119057', 'admin@gmail.com', 'admin', '4297f44b13955235245b2497399d7a93'),
(112, 'Ariq Ali Saman', 'Serpong, Tangerang Selatan', '089696474710', 'arqsamn@gmail.com', 'arqsamn', 'fae0b27c451c728867a567e8c1bb4e53'),
(115, 'Robby Suganda', 'Pondok Jagung, Tangerang Selatan', '082112481600', 'robbysugandaa@gmail.com', 'bee', '1e48c4420b7073bc11916c6c1de226bb'),
(116, 'Wirahadi Tjahjadi', 'Ciledug, Tangerang', '089635531902', 'wira@gmail.com', 'wira', '731309c4bb223491a9f67eac5214fb2e');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_spk`
--

CREATE TABLE `hasil_spk` (
  `id_spk` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `hasil_spk` float(10,2) DEFAULT NULL,
  `periode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hasil_spk`
--

INSERT INTO `hasil_spk` (`id_spk`, `id_karyawan`, `hasil_spk`, `periode`) VALUES
(491, 28, 48.00, '2023-08-01 / 2023-08-31'),
(492, 16, 48.00, '2023-10-01 / 2023-10-31'),
(493, 14, 79.00, '2023-07-01 / 2023-07-31'),
(494, 14, 82.00, '2023-06-01 / 2023-06-30'),
(495, 15, 91.00, '2023-06-01 / 2023-06-30'),
(496, 16, 93.00, '2023-06-01 / 2023-06-30'),
(497, 17, 66.00, '2023-06-01 / 2023-06-30'),
(498, 18, 80.50, '2023-06-01 / 2023-06-30'),
(499, 19, 73.50, '2023-06-01 / 2023-06-30'),
(500, 20, 79.00, '2023-06-01 / 2023-06-30'),
(501, 21, 78.00, '2023-06-01 / 2023-06-30'),
(502, 22, 90.50, '2023-06-01 / 2023-06-30'),
(503, 23, 98.50, '2023-06-01 / 2023-06-30'),
(504, 24, 83.00, '2023-06-01 / 2023-06-30'),
(505, 25, 79.00, '2023-06-01 / 2023-06-30'),
(506, 27, 86.50, '2023-06-01 / 2023-06-30'),
(507, 28, 82.00, '2023-06-01 / 2023-06-30'),
(508, 29, 78.50, '2023-06-01 / 2023-06-30'),
(509, 30, 86.00, '2023-06-01 / 2023-06-30'),
(510, 31, 66.00, '2023-06-01 / 2023-06-30'),
(511, 32, 58.50, '2023-06-01 / 2023-06-30'),
(512, 33, 87.00, '2023-06-01 / 2023-06-30'),
(513, 34, 87.50, '2023-06-01 / 2023-06-30'),
(514, 35, 76.00, '2023-06-01 / 2023-06-30'),
(515, 36, 71.50, '2023-06-01 / 2023-06-30'),
(516, 37, 83.00, '2023-06-01 / 2023-06-30'),
(517, 38, 83.50, '2023-06-01 / 2023-06-30'),
(518, 39, 89.00, '2023-06-01 / 2023-06-30'),
(519, 40, 79.00, '2023-06-01 / 2023-06-30'),
(520, 41, 75.50, '2023-06-01 / 2023-06-30'),
(521, 42, 75.50, '2023-06-01 / 2023-06-30'),
(522, 55, 88.50, '2023-06-01 / 2023-06-30'),
(523, 57, 68.50, '2023-06-01 / 2023-06-30'),
(524, 58, 55.50, '2023-06-01 / 2023-06-30'),
(525, 59, 66.50, '2023-06-01 / 2023-06-30'),
(526, 60, 86.50, '2023-06-01 / 2023-06-30'),
(527, 61, 85.50, '2023-06-01 / 2023-06-30'),
(528, 62, 83.00, '2023-06-01 / 2023-06-30'),
(529, 63, 75.50, '2023-06-01 / 2023-06-30'),
(530, 64, 90.00, '2023-06-01 / 2023-06-30'),
(531, 65, 82.50, '2023-06-01 / 2023-06-30'),
(532, 66, 74.00, '2023-06-01 / 2023-06-30'),
(533, 67, 69.50, '2023-06-01 / 2023-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `NIK` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `pendidikan_terakhir` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `tanggal_bergabung` date NOT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `pengalaman` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `NIK`, `nama`, `jenis_kelamin`, `alamat`, `telepon`, `tanggal_lahir`, `tempat_lahir`, `pendidikan_terakhir`, `jabatan`, `tanggal_bergabung`, `skill`, `pengalaman`) VALUES
(14, '3674271020221001', 'Ariq Ali Saman', 'Pria', 'Serpong, Tangerang Selatan', '089696474710', '2002-10-18', 'Jakarta', 'S1', 'Front End', '2022-09-07', 'Laravel', ''),
(15, '3674271020221002', 'Nurul Azzahra', 'Wanita', 'Pamulang, Tangerang Selatan', '081286616893', '2000-10-08', 'Jakarta', 'S1', 'Quality Assurance', '2022-09-05', 'Katalon', ''),
(16, '3674271020221003', 'Simbar Madani', 'Pria', 'Gading  Serpong, Tangerang Selatan', '083870815121', '1994-09-16', 'Jakarta', 'S1', 'Back End', '2022-09-06', 'Laravel dan CI', ''),
(17, '3674271020221004', 'Amirul Fatah', 'Pria', 'Sawangan, Depok', '08971506779', '2002-07-15', 'Depok', 'S1', 'Front End', '2022-09-05', '', ''),
(18, '3674271020221005', 'Irennada', 'Wanita', 'Pondok Aren, Tangerang Selatan', '089694938291', '2000-04-30', 'Jakarta', 'S1', 'Quality Assurance', '2022-09-05', '', ''),
(19, '3674271020221006', 'Try Wathoriq', 'Pria', 'Ciledug, Tangerang', '085240233277', '2000-08-10', 'Jakarta', 'S1', 'Front End', '2022-09-07', '', ''),
(20, '3674271020221007', 'Indirahma Putri', 'Wanita', 'Ciputat, Tangerang Selatan', '085399074249', '2000-02-25', 'Jakarta', 'S1', 'Quality Assurance', '2022-10-23', '', ''),
(21, '3674271020221008', 'Andrew Rizky Prabowo', 'Pria', 'Ciputat, Tangerang Selatan', '081286616893', '2000-11-02', 'Jakarta', 'S1', 'Back End', '2022-02-08', '', ''),
(22, '3674271020221009', 'Vira Hidayanti', 'Wanita', 'Sawangan, Depok', '089604215643', '1999-07-27', 'Jakarta', 'S1', 'Quality Assurance', '2022-04-22', '', ''),
(23, '3674271020221010', 'Dicky Damanhuri', 'Pria', 'Serpong, Tangerang Selatan', '085257344607', '2002-03-04', 'Tangerang', 'S1', 'Data Analyst', '2022-09-07', '', ''),
(24, '3674271020221011', 'Fauzan Akmal Baihaqi', 'Pria', 'Bintaro, Tangerang Selatan', '082113720772 ', '2002-11-11', 'Jakarta', 'S1', 'Back End', '2022-08-24', '', ''),
(25, '3674271020221012', 'Alvina Aulia', 'Wanita', 'Cirendeu, Tangerang Selatan', '085776422447', '2001-01-04', 'Tangerang', 'S1', 'Front End', '2022-09-07', '', ''),
(27, '3674271020221013', 'Faradibba Maiwa', 'Wanita', 'Ciputat, Tangerang Selatan', '081219550856 ', '1999-12-18', 'Tangerang', 'S1', 'Data Analyst', '2022-11-22', '', ''),
(28, '3674271020221014', 'Adam Syah', 'Pria', 'Serpong, Tangerang Selatan', '085213931566', '2002-09-13', 'Tangerang', 'S1', 'Data Analyst', '2022-09-06', '', ''),
(29, '3674271020221015', 'Muklis', 'Pria', 'Pamulang, Tangerang Selatan', '082193947112', '1983-07-22', 'Tangerang', 'S2', 'Back End', '2021-03-02', '', ''),
(30, '3674271020221016', 'Muhammad Wahyu Aditya', 'Pria', 'Serpong, Tangerang Selatan', '085776422447', '2002-09-18', 'Tangerang', 'S1', 'Back End', '2022-09-08', '', ''),
(31, '3674271020221017', 'Mohamad Naufal Ghifari', 'Pria', 'Serpong, Tangerang Selatan', '081282757167', '2002-12-30', 'Tangerang', 'S1', 'Quality Assurance', '2022-10-04', '', ''),
(32, '3674271020221018', 'Fityan Hanif Assalmi', 'Pria', 'Serpong, Tangerang Selatan', '087888707222', '2002-06-01', 'Tangerang', 'S1', 'Data Analyst', '2022-11-24', '', ''),
(33, '3674271020221019', 'Nur Afifah Husna', 'Wanita', 'Serpong, Tangerang Selatan', '085718077859', '2001-07-03', 'Jakarta', 'S1', 'Data Analyst', '2022-07-14', '', ''),
(34, '3674271020221020', 'Muammar Evin Ferdiansyah', 'Pria', 'Pondok Aren, Tangerang Selatan', '081802071973', '2002-02-15', 'Jakarta', 'S1', 'Front End', '2022-10-25', '', ''),
(35, '3674271020221021', 'Tiara Savitri', 'Wanita', 'Pondok Cabe, Tangerang Selatan', '081514873157', '2000-08-19', 'Jakarta', 'S1', 'Quality Assurance', '2022-10-20', '', ''),
(36, '3674271020221022', 'Layla Azizah', 'Wanita', 'Cirendeu, Tangerang Selatan', '082179708320', '2000-08-30', 'Tangerang', 'S1', 'Quality Assurance', '2022-11-15', '', ''),
(37, '3674271020221023', 'Muhammad Ilhamsyah', 'Pria', 'Ciputat, Tangerang Selatan', '085772095161', '2000-08-06', 'Tangerang', 'S1', 'Front End', '2022-06-28', '', ''),
(38, '3674271020221024', 'Dwi Rizki Rismawanto', 'Pria', 'Pamulang, Tangerang Selatan', '087808001917', '2002-07-19', 'Tangerang', 'S1', 'Back End', '2022-03-07', '', ''),
(39, '3674271020221025', 'Rizki Seftiyan', 'Pria', 'Pamulang, Tangerang Selatan', '081210208589', '2002-09-05', 'Brebes', 'S1', 'Data Analyst', '2022-10-26', '', ''),
(40, '3674271020221026', 'Renata Suyudi', 'Wanita', 'Ciputat, Tangerang Selatan', '081213501165', '2000-05-29', 'Jakarta', 'S1', 'Front End', '2022-01-06', '', ''),
(41, '3674271020221027', 'Muhammad Haikal Fadlurrahman', 'Pria', 'Cisauk, Tangerang Selatan', '087887714321', '2001-08-10', 'Tangerang', 'S1', 'Back End', '2022-02-19', '', ''),
(42, '3674271020221028', 'Saiful Jihad Fatahilah', 'Pria', 'Magelang, Jawa Tengah', '081384957099', '2001-10-01', 'Magelang', 'S1', 'Front End', '2022-01-21', '', ''),
(55, '3674271020221029', 'Sabrina Azzahara', 'Wanita', 'Ciputat, Tangerang Selatan', '085780091512', '2000-07-25', 'Jakarta', 'S1', 'Quality Assurance', '2022-05-16', '', ''),
(57, '3674271020221030', 'Aufa Wira', 'Pria', 'Pondok Betung, Tangerang Selatan', '087876907543', '2023-05-21', 'Bandung', 'S3', 'Data Analyst', '2023-05-05', '', ''),
(58, '3674271020221031', 'Muhammad Ghazy', 'Pria', 'Pondok Cabe, Tangerang Selatan', '0896252543425', '1999-09-24', 'Tangerang', 'S1', 'Data Analyst', '2023-03-16', '', ''),
(59, '3674271020221032', 'Made Nugraha', 'Pria', 'Cempaka Putih, Jakarta Pusat', '0857765556456', '2000-12-16', 'Jakarta', 'S1', 'Front End', '2022-10-25', '', ''),
(60, '3674271020221033', 'Giovanni Risrian', 'Pria', 'Parung, Bogor', '081513157378', '1998-05-09', 'Bogor', 'S1', 'Back End', '2022-04-27', '', ''),
(61, '3674271020221034', 'Annisa Rafida', 'Wanita', 'Pondok Aren, Tangerang Selatan', '08111116578', '2001-01-03', 'Jakarta', 'S1', 'Quality Assurance', '2023-02-16', '', ''),
(62, '3674271020221035', 'Mentari Khairunnisa', 'Wanita', 'Tebet, Jakarta Selatan', '085819877091', '2000-05-10', 'Jakarta', 'S1', 'Data Analyst', '2022-09-03', '', ''),
(63, '3674271020221036', 'Muhammad Uwais Al-Fatih', 'Pria', 'Tanah Abang, Jakarta Pusat', '081586965685', '2000-05-26', 'Jakarta', 'S1', 'Front End', '2022-09-12', '', ''),
(64, '3674271020221037', 'Aisyah Ruqoyah', 'Wanita', 'Kemang, Jakarta Selatan', '089675052628', '2001-06-11', 'Jakarta', 'S1', 'Quality Assurance', '2022-11-30', '', ''),
(65, '3674271020221038', 'Putri Utami', 'Wanita', 'Gunung Sindur, Bogor', '08126724465', '1998-12-26', 'Bogor', 'S1', 'Data Analyst', '2022-01-17', '', ''),
(66, '3674271020221039', 'Anggita Mukti', 'Wanita', 'Pondok Jagung, Tangerang Selatan', '085715805745', '2001-07-29', 'Tangerang', 'S1', 'Front End', '2023-01-17', '', ''),
(67, '3674271020221040', 'Timothy Marvel', 'Pria', 'Alam Sutera, Tangerang Selatan', '081213560632', '1999-09-27', 'Jakarta', 'S1', 'Back End', '2022-07-26', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `kriteria` varchar(32) DEFAULT NULL,
  `bobot` float(5,2) DEFAULT NULL,
  `type` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `kriteria`, `bobot`, `type`) VALUES
(1, 'Absensi', 15.00, 'Benefit'),
(2, 'Kerjasama', 40.00, 'Benefit'),
(3, 'Kerapihan', 20.00, 'Benefit'),
(4, 'Keaktifan', 25.00, 'Benefit');

-- --------------------------------------------------------

--
-- Table structure for table `penilaian_karyawan`
--

CREATE TABLE `penilaian_karyawan` (
  `id_test` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `Absensi` int(11) DEFAULT NULL,
  `Kerjasama` int(11) DEFAULT NULL,
  `Kerapihan` int(11) DEFAULT NULL,
  `Keaktifan` int(11) DEFAULT NULL,
  `periode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `penilaian_karyawan`
--

INSERT INTO `penilaian_karyawan` (`id_test`, `id_karyawan`, `Absensi`, `Kerjasama`, `Kerapihan`, `Keaktifan`, `periode`) VALUES
(62, 14, 87, 99, 103, 110, '2023-06-01 / 2023-06-30'),
(63, 15, 87, 98, 104, 109, '2023-06-01 / 2023-06-30'),
(66, 16, 89, 98, 104, 108, '2023-06-01 / 2023-06-30'),
(67, 17, 89, 100, 103, 111, '2023-06-01 / 2023-06-30'),
(68, 18, 88, 99, 103, 110, '2023-06-01 / 2023-06-30'),
(69, 19, 90, 98, 106, 110, '2023-06-01 / 2023-06-30'),
(72, 20, 89, 99, 103, 110, '2023-06-01 / 2023-06-30'),
(73, 21, 91, 100, 103, 108, '2023-06-01 / 2023-06-30'),
(74, 22, 90, 98, 103, 109, '2023-06-01 / 2023-06-30'),
(75, 23, 88, 98, 103, 108, '2023-06-01 / 2023-06-30'),
(76, 24, 87, 99, 104, 109, '2023-06-01 / 2023-06-30'),
(77, 25, 87, 100, 103, 109, '2023-06-01 / 2023-06-30'),
(78, 27, 88, 99, 104, 108, '2023-06-01 / 2023-06-30'),
(79, 28, 91, 98, 106, 108, '2023-06-01 / 2023-06-30'),
(80, 29, 88, 100, 104, 108, '2023-06-01 / 2023-06-30'),
(81, 30, 91, 98, 105, 108, '2023-06-01 / 2023-06-30'),
(82, 31, 87, 101, 103, 110, '2023-06-01 / 2023-06-30'),
(83, 32, 90, 101, 105, 109, '2023-06-01 / 2023-06-30'),
(84, 33, 87, 99, 103, 109, '2023-06-01 / 2023-06-30'),
(85, 34, 92, 98, 103, 109, '2023-06-01 / 2023-06-30'),
(86, 35, 87, 101, 103, 108, '2023-06-01 / 2023-06-30'),
(87, 36, 92, 100, 103, 109, '2023-06-01 / 2023-06-30'),
(88, 37, 93, 98, 105, 108, '2023-06-01 / 2023-06-30'),
(89, 38, 90, 99, 104, 108, '2023-06-01 / 2023-06-30'),
(90, 39, 89, 99, 103, 108, '2023-06-01 / 2023-06-30'),
(91, 40, 89, 99, 103, 110, '2023-06-01 / 2023-06-30'),
(92, 41, 88, 99, 103, 111, '2023-06-01 / 2023-06-30'),
(93, 42, 92, 99, 104, 109, '2023-06-01 / 2023-06-30'),
(94, 55, 88, 98, 103, 110, '2023-06-01 / 2023-06-30'),
(95, 57, 90, 99, 104, 111, '2023-06-01 / 2023-06-30'),
(96, 58, 92, 100, 107, 109, '2023-06-01 / 2023-06-30'),
(97, 59, 92, 100, 103, 110, '2023-06-01 / 2023-06-30'),
(98, 60, 90, 98, 104, 109, '2023-06-01 / 2023-06-30'),
(99, 61, 90, 98, 103, 110, '2023-06-01 / 2023-06-30'),
(100, 62, 93, 98, 105, 108, '2023-06-01 / 2023-06-30'),
(101, 63, 90, 100, 104, 108, '2023-06-01 / 2023-06-30'),
(102, 64, 87, 98, 103, 110, '2023-06-01 / 2023-06-30'),
(103, 65, 88, 100, 103, 108, '2023-06-01 / 2023-06-30'),
(104, 66, 87, 100, 103, 110, '2023-06-01 / 2023-06-30'),
(113, 67, 92, 99, 103, 111, '2023-06-01 / 2023-06-30'),
(114, 14, 93, 98, 106, 108, '2023-07-01 / 2023-07-31'),
(125, 28, 87, 102, 103, 112, '2023-08-01 / 2023-08-31'),
(127, 58, 87, 98, 103, 108, '2023-09-01 / 2023-09-30'),
(132, 16, 87, 102, 103, 112, '2023-10-01 / 2023-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kriteria`
--

CREATE TABLE `sub_kriteria` (
  `id_subkriteria` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `subkriteria` varchar(255) NOT NULL,
  `nilai` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_kriteria`
--

INSERT INTO `sub_kriteria` (`id_subkriteria`, `id_kriteria`, `subkriteria`, `nilai`) VALUES
(87, 1, 'Tidak pernah terlambat', 10.00),
(88, 1, 'Terlambat 1 kali', 9.00),
(89, 1, 'Terlambat 2 kali', 8.00),
(90, 1, 'Terlambat 3 kali', 7.00),
(91, 1, 'Terlambat 4 kali', 6.00),
(92, 1, 'Terlambat 5 kali', 5.00),
(93, 1, 'Terlambat 6 kali', 4.00),
(94, 1, 'Terlambat 7 kali', 3.00),
(95, 1, 'Terlambat 8 kali', 2.00),
(96, 1, 'Terlambat 9 kali', 1.00),
(97, 1, 'Terlambat  10 kali atau lebih', 0.00),
(98, 2, 'Baik Sekali', 10.00),
(99, 2, 'Baik', 8.00),
(100, 2, 'Cukup', 6.00),
(101, 2, 'Kurang', 4.00),
(102, 2, 'Kurang Sekali', 2.00),
(103, 3, 'Baik Sekali', 10.00),
(104, 3, 'Baik', 8.00),
(105, 3, 'Cukup', 6.00),
(106, 3, 'Kurang', 4.00),
(107, 3, 'Kurang Sekali', 2.00),
(108, 4, 'Baik Sekali', 10.00),
(109, 4, 'Baik', 8.00),
(110, 4, 'Cukup', 6.00),
(111, 4, 'Kurang', 4.00),
(112, 4, 'Kurang Sekali', 2.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_spk`
--
ALTER TABLE `hasil_spk`
  ADD PRIMARY KEY (`id_spk`),
  ADD KEY `id_calon_kr` (`id_karyawan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `penilaian_karyawan`
--
ALTER TABLE `penilaian_karyawan`
  ADD PRIMARY KEY (`id_test`),
  ADD KEY `id_calon_kr` (`id_karyawan`);

--
-- Indexes for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD PRIMARY KEY (`id_subkriteria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `hasil_spk`
--
ALTER TABLE `hasil_spk`
  MODIFY `id_spk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=534;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `penilaian_karyawan`
--
ALTER TABLE `penilaian_karyawan`
  MODIFY `id_test` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  MODIFY `id_subkriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
