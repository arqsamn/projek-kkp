<?php
session_start();
error_reporting(0);
if (empty($_SESSION['id'])) {
    header('location:login_page.php?error_login=1');
}
?>
<?php
include 'db/db_config.php';
$host = "localhost";
$dbname = "projek_kkp";
$username = "root";
$password = "";
try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname;", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
}

$periode = $_POST['periode'];
$explode = explode('/', $_POST['periode']);

// $stmt = $conn->prepare("SELECT * FROM penilaian_karyawan WHERE periode='$periode'");
$stmt = $conn->prepare("SELECT karyawan.nama, penilaian_karyawan.* 
FROM karyawan 
JOIN penilaian_karyawan 
ON karyawan.id_karyawan = penilaian_karyawan.id_karyawan  
WHERE periode='$periode'
");

$stmt->execute();

// set the resulting array to associative
$result = $stmt->fetchAll();

extract($_POST);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/logo_mnm.png">
    <title>Proses SPK</title>
    <link rel="stylesheet" type="text/css" href="assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css">
    <!-- Custom CSS -->
    <link href="dist/css/fix-style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-default fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <?php include 'layouts/loader.php' ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include 'layouts/header.php' ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User Profile-->
                <?php include 'layouts/sidebar.php' ?>
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Proses SPK</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                                <li class="breadcrumb-item active">Proses SPK</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($_GET['error_msg'])) : ?>
                            <div class="alert alert-danger">
                                <?= $_GET['error_msg']; ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form method="post">
                                    <div class="row col-md-12">
                                        <div class="col-md-6">
                                            <label for="nama"><b>Periode</b></label>
                                            <input id="periode" type="text" class="form-control" name="periode" value="" placeholder="Input Date" required />
                                        </div>
                                        <div class="col-md-4 m-t-30">
                                            <!-- <button type="submit" class="btn btn-success filter">Submit</button> -->
                                            <button id="hf" class="btn btn-success"><i class=" ti-search"></i> Cari</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive m-t-20">
                                    <h4><b>Tabel Hasil Penilaian</b></h4>
                                    <table id="example2" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Nama Karyawan</th>
                                                <?php foreach ($db->select('kriteria', 'kriteria')->get() as $k) : ?>
                                                    <th>
                                                        <?php
                                                        $tmp = explode('_', $k['kriteria']);
                                                        echo ucwords(implode(' ', $tmp));
                                                        ?>
                                                    </th>
                                                <?php endforeach ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // foreach ($db->select('karyawan.nama,penilaian_karyawan.*', 'karyawan,penilaian_karyawan')->where('karyawan.id_karyawan=penilaian_karyawan.id_karyawan')->get() as $data) :
                                            foreach ($result as $data) :
                                            ?>
                                                <tr>
                                                    <td><?= $data['nama'] ?></td>
                                                    <?php foreach ($db->select('kriteria', 'kriteria')->get() as $td) : ?>
                                                        <td><?= $db->getnilaisubkriteria($data[$td['kriteria']]) ?></td>
                                                    <?php endforeach ?>
                                                </tr>
                                            <?php
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <button id=" sk" class="btn btn-info m-t-20" style="margin-left: 10cm;" onclick="tpl()"><i class="ti-check-box"></i> Proses</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="proses_spk" style="display: none;">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h4><b>Normalisasi</b></h4>
                                        <table id="example2" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Nama Karyawan</th>
                                                    <?php foreach ($db->select('kriteria', 'kriteria')->get() as $k) : ?>
                                                        <th>
                                                            <?php
                                                            $tmp = explode('_', $k['kriteria']);
                                                            echo ucwords(implode(' ', $tmp));
                                                            ?>
                                                        </th>
                                                    <?php endforeach ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                // foreach ($db->select('karyawan.nama,penilaian_karyawan.*', 'karyawan,penilaian_karyawan')->where('karyawan.id_karyawan=penilaian_karyawan.id_karyawan')->get() as $data) :
                                                foreach ($result as $data) :
                                                ?>
                                                    <tr>
                                                        <td><?= $data['nama'] ?></td>
                                                        <?php foreach ($db->select('kriteria', 'kriteria')->get() as $td) : ?>
                                                            <td><?= number_format($db->rumus($db->getnilaisubkriteria($data[$td['kriteria']]), $td['kriteria']), 2); ?></td>
                                                        <?php endforeach ?>
                                                    </tr>
                                                <?php
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h4><b>Proses Penentuan</b></h4>
                                        <table id="example3" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Nama </th>
                                                    <th>Hasil</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                // foreach ($db->select('karyawan.id_karyawan,karyawan.nama,penilaian_karyawan.*', 'karyawan,penilaian_karyawan')->where('karyawan.id_karyawan=penilaian_karyawan.id_karyawan')->get() as $data) :
                                                foreach ($result as $data) :
                                                ?>
                                                    <tr>
                                                        <td><?= $data['nama'] ?></td>
                                                        <td>
                                                            <?php
                                                            $hasil = [];
                                                            foreach ($db->select('kriteria', 'kriteria')->get() as $dt) {
                                                                array_push($hasil, $db->rumus($db->getnilaisubkriteria($data[$dt['kriteria']]), $dt['kriteria']) * $db->bobot($dt['kriteria']));
                                                            }
                                                            echo $h = number_format(array_sum($hasil), 2);
                                                            if ($db->select('id_karyawan', 'hasil_spk')->where("id_karyawan='$data[id_karyawan]' and periode='$periode'")->count() == 0) {
                                                                $db->insert('hasil_spk', "'$data[id_karyawan]','$h','$periode'")->count();
                                                            } else {
                                                                $db->update('hasil_spk', "hasil_spk='$h',periode='$periode'")->where("id_karyawan='$data[id_karyawan]' and periode='$periode'")->count();
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h4><b>Perangkingan</b></h4>
                                        <table id="example4" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Hasil </th>
                                                    <?php $no = 1;
                                                    foreach ($db->select('kriteria', 'kriteria')->get() as $th) : ?>
                                                        <th>K<?= $no ?></th>
                                                    <?php $no++;
                                                    endforeach ?>
                                                    <th rowspan="2" style="padding-bottom:25px">Hasil</th>
                                                    <th rowspan="2" style="padding-bottom:25px">Ranking</th>
                                                </tr>
                                                <tr>
                                                    <th>Bobot </th>
                                                    <?php foreach ($db->select('bobot', 'kriteria')->get() as $th) : ?>
                                                        <th><?= $th['bobot'] ?></th>
                                                    <?php endforeach ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 1;
                                                // $bulan = date('m');
                                                // $tahun = date('Y');
                                                // $tanggal = date('Y-m-d');
                                                // $minggu = $db->weekOfMonth($tanggal);

                                                // $stmt_ku = $conn->prepare("SELECT DISTINCT karyawan.nama, penilaian_karyawan.*, hasil_spk.* FROM karyawan JOIN penilaian_karyawan ON karyawan.id_karyawan = penilaian_karyawan.id_karyawan JOIN hasil_spk ON penilaian_karyawan.id_karyawan = hasil_spk.id_karyawan WHERE hasil_spk.periode='$periode' ORDER BY hasil_spk.hasil_spk DESC");
                                                $stmt_ku = $conn->prepare("SELECT karyawan.nama, penilaian_karyawan.*, hasil_spk.*
                                                FROM karyawan
                                                JOIN hasil_spk ON karyawan.id_karyawan = hasil_spk.id_karyawan
                                                JOIN penilaian_karyawan ON hasil_spk.id_karyawan = penilaian_karyawan.id_karyawan
                                                WHERE hasil_spk.periode = '$periode'
                                                GROUP BY karyawan.nama, hasil_spk.hasil_spk
                                                ORDER BY hasil_spk.hasil_spk DESC
                                                ");

                                                $stmt_ku->execute();

                                                $result_ku = $stmt_ku->fetchAll();
                                                // var_dump($result_ku);
                                                // exit();
                                                // foreach ($db->select('distinct(karyawan.nama),penilaian_karyawan.*,hasil_spk.*', 'karyawan,penilaian_karyawan,hasil_spk')->where('karyawan.id_karyawan=penilaian_karyawan.id_karyawan and karyawan.id_karyawan=hasil_spk.id_karyawan and hasil_spk.minggu=' . $minggu . ' and hasil_spk.bulan=' . $bulan . ' and hasil_spk.tahun=' . $tahun . ' and hasil_spk.periode=' . $periode)->order_by('hasil_spk.hasil_spk', 'desc')->get() as $data) :
                                                foreach ($result_ku as $data) :
                                                ?>
                                                    <tr>
                                                        <td><?= $data['nama'] ?></td>
                                                        <?php foreach ($db->select('kriteria', 'kriteria')->get() as $td) : ?>
                                                            <td><?= number_format($db->rumus($db->getnilaisubkriteria($data[$td['kriteria']]), $td['kriteria']), 2); ?></td>
                                                        <?php endforeach ?>
                                                        <td>
                                                            <?php
                                                            $hasil = [];
                                                            foreach ($db->select('kriteria', 'kriteria')->get() as $dt) {
                                                                array_push($hasil, $db->rumus($db->getnilaisubkriteria($data[$dt['kriteria']]), $dt['kriteria']) * $db->bobot($dt['kriteria']));
                                                            }
                                                            echo $h = number_format(array_sum($hasil), 2);
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= $no ?>
                                                        </td>
                                                    </tr>
                                                <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <?php include 'layouts/custom_style.php' ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <?php include 'layouts/footer.php' ?>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/popper/popper.min.js"></script>
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="assets/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(function() {
            var startDate;
            var endDate;
            $(document).ready(function() {
                $('#periode').daterangepicker({
                        showDropdowns: true,
                        startDate: moment(),
                        endDate: moment(),
                        locale: {
                            "format": "YYYY-MM-DD",
                            "separator": " / ",
                            "applyLabel": "Apply",
                            "cancelLabel": "Cancel",
                            "fromLabel": "From",
                            "toLabel": "To",
                            "customRangeLabel": "Custom",
                            "weekLabel": "W",
                            "daysOfWeek": [
                                "Su",
                                "Mo",
                                "Tu",
                                "We",
                                "Th",
                                "Fr",
                                "Sa"
                            ],
                            monthNames: [
                                "January",
                                "February",
                                "March",
                                "April",
                                "May",
                                "June",
                                "July",
                                "August",
                                "September",
                                "October",
                                "November",
                                "December"
                            ],
                            firstDay: 1
                        },
                    },
                    function(start, end) {
                        // console.log(start.format('DD MMMM YYYY') + ' - ' + end.format(
                        //     'DD MMMM YYYY'));
                        $('#periode').html(start.format('DD MMMM YYYY') + ' - ' + end.format('DD MMMM YYYY'));
                        startDate = start;
                        endDate = end;
                        FilterPeriode(
                            start.format('YYYY-MM-DD'),
                            end.format('YYYY-MM-DD'),
                        );
                    }
                );
                $('#periode').html(moment().format('DD MMMM YYYY') + ' - ' + moment()
                    .format('DD MMMM YYYY'));

                $('#saveBtn').click(function() {
                    //console.log(startDate.format('DD MMMM YYYY') + ' - ' + endDate.format('DD MMMM YYYY'));
                    $('#tampil').html(startDate.format('DD MMMM YYYY') + ' - ' + endDate.format('DD MMMM YYYY'));
                });

                function FilterPeriode(startDate, endDate) {
                    var DateRange = startDate + '-' + endDate;

                    $.ajax({
                        type: "GET",
                        url: "data_penilaian_hasil.php",
                        data: {
                            startDate: startDate,
                            endDate: endDate
                        },
                    });
                    console.log(startDate, endDate);
                }
            });
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });
    </script>
    <script type="text/javascript">
        function tpl() {
            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Proses SPK Berhasil!',
                showConfirmButton: true,
                timer: 3000
            }).then((result) => {
                $("#proses_spk").show();
            })
        }
    </script>
</body>

</html>